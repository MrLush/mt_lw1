package com.example.springposgres.repository;

import java.util.List;

import com.example.springposgres.model.Customer;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
	List<Customer> findByFirstName(String FirstName);
	List<Customer> findAll();
}