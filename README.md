Java laboratory work

This project was created to explore the possibilities provided by spring boot and gradle.

Calling the method of adding customers to the DB:

![Test 1](/img/Screenshot_1.png "Test 1")

Result of getting cusomers from the DB:

![Test 2](/img/Screenshot_2.png "Test 2")

Search result of a cusomer by name from the DB:

![Test 3](/img/Screenshot_3.png "Test 3")